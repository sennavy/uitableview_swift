//
//  SampleCollectionCell.swift
//  UITableView
//
//  Created by Navy on 19/9/22.
//

import UIKit

class SampleCollectionCell: UICollectionViewCell {
    @IBOutlet weak var appIconImg   : UIImageView!
    @IBOutlet weak var appTitle     : UILabel!
    
    
    func configCell(imgString: String, title: String) {
        appIconImg.image = UIImage(named: imgString)
        appTitle.text = title
    }
}
