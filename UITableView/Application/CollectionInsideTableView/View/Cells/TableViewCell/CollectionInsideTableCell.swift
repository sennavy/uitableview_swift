//
//  CollectionInsideTableCell.swift
//  UITableView
//
//  Created by Navy on 19/9/22.
//

import UIKit

class CollectionInsideTableCell: UITableViewCell {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK: - Variables
    let appInfoVM = AppInfoVM()
    
    
    // MARK: - LifeCyle
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        appInfoVM.initCellData()
    }
}


// MARK: - CollectionView
extension CollectionInsideTableCell: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appInfoVM.cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = self.appInfoVM.cells[indexPath.row].value as! AppInfo
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SampleCollectionCell", for: indexPath) as! SampleCollectionCell
        cell.configCell(imgString: data.image_str, title: data.title)
        return cell
    }

}
