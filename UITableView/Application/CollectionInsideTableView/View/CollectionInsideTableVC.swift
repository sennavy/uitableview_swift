//
//  CollectionInsideTableVC.swift
//  UITableView
//
//  Created by Navy on 19/9/22.
//

import UIKit


enum AppRowType: Int, CaseIterable {
    case Suggested
    case Popular
}

class CollectionInsideTableVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


// MARK: - UITableView
extension CollectionInsideTableVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = AppRowType(rawValue: indexPath.row)
        switch row {
        case .Suggested:
            return UITableView.automaticDimension
            
        default:
            return 200
        }
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppRowType.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = AppRowType(rawValue: indexPath.row)
        
        switch row {
        case .Suggested:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BigSuggestionCell", for: indexPath) as! BigSuggestionCell
            return cell
         
       
        default: // case Popular
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionInsideTableCell", for: indexPath) as! CollectionInsideTableCell
            return cell
        }
    }
}

