//
//  CollectionInsideTableModel.swift
//  UITableView
//
//  Created by Navy on 21/9/22.
//

import Foundation
// big array model to store all app
struct AppInfoModel <T> {
    var value        : T?
}

// properties of each contact
struct AppInfo {
    var image_str : String
    var title     : String
}
