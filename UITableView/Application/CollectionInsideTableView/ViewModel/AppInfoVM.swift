//
//  CollectionInsideTableVM.swift
//  UITableView
//
//  Created by Navy on 21/9/22.
//

import Foundation
class AppInfoVM {
    
    // variables
    var cells : [AppInfoModel<Any>] = []
    
    // functions
    func initCellData() {
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_4", title: "Call of Duty")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_5", title: "The Sniper")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_6", title: "Black Fighter")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_7", title: "Legend of Sword")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_8", title: "The Lone Warrior")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_1", title: "League of Legend")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_2", title: "Ninja Shadow")))
        cells.append(AppInfoModel<Any>(value: AppInfo(image_str: "game_3", title: "Mortal Combat")))
    }
    
}
