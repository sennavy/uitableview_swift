//
//  ReusingXibFileVC.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import UIKit

class ReusingXibFileVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // must register nib file before being used
        tableView.register(UINib(nibName: "ReusableXibFile", bundle: nil), forCellReuseIdentifier: "ReusableXibCell")
    }

}

// MARK: - UITableView
extension ReusingXibFileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableXibCell", for: indexPath) as! ReusableXibCell
        return cell
    }

}
