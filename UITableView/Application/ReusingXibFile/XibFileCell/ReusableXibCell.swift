//
//  ReusableXibCell.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import UIKit

class ReusableXibCell: UITableViewCell {

    @IBOutlet weak var titleLabel      : UILabel!
    @IBOutlet weak var containerView   : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
