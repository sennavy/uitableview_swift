//
//  TableViewSectionVC.swift
//  UITableView
//
//  Created by Navy on 23/9/22.
//

import UIKit

enum MenuSections: Int, CaseIterable {
    case PopularNow
    case Food
    case Desert
    case Drink
}

class TableViewSectionVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }

}

// MARK: - UItableView
extension TableViewSectionVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let sections = MenuSections(rawValue: section)
        switch sections {
        case .PopularNow: return "Popular Now"
        case .Food:       return "Food"
        case .Desert:     return "Desert"
        case .Drink:      return "Drink"
        default:          return ""
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return MenuSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodMenuListCell", for: indexPath) as! FoodMenuListCell

        let sections = MenuSections(rawValue: indexPath.section)
        switch sections {
        case .PopularNow:
            cell.configCell(image: "food_8", title: "Fried Chicken", price: "$2.30")
            
        case .Food:
            cell.configCell(image: "food_6", title: "Mixed Vegan", price: "$2.30")
            
        case .Desert:
            cell.configCell(image: "des_1", title: "Vanilla Cake", price: "$2.30")
            
        case .Drink:
            cell.configCell(image: "drink_1", title: "Strawberry Frapped", price: "$2.30")
            
        default:
            return UITableViewCell()
        }
    
        return cell
    }
    
    
}
