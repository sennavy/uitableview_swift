//
//  FoodMenuListCell.swift
//  UITableView
//
//  Created by Navy on 23/9/22.
//

import UIKit

class FoodMenuListCell: UITableViewCell {

    @IBOutlet weak var foodImage    : UIImageView!
    @IBOutlet weak var nameLabel         : UILabel!
    @IBOutlet weak var priceLabel        : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configCell(image: String, title: String, price: String) {
        foodImage.image = UIImage(named: image)
        nameLabel.text = title
        priceLabel.text = price
    }

}
