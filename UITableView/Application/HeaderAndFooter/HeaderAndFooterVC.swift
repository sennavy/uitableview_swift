//
//  HeaderAndFooterVC.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import UIKit

class HeaderAndFooterVC: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        // add space for footer and header of table View
        var insets: UIEdgeInsets = tableView.contentInset
        insets.bottom = 100
        insets.top = 100
        tableView.contentInset = insets
        
        super.viewDidLoad()
    }
}


extension HeaderAndFooterVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderAndFooterCell", for: indexPath) as! HeaderAndFooterCell
        return cell
    }
    
    
}
