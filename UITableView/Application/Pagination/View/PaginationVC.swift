//
//  PaginationVC.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import UIKit

class PaginationVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    let paginationVM = PaginationVM()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create cell
        paginationVM.initCellData()
        
        // register nib file
        tableView.register(UINib(nibName: "ReusableXibFile", bundle: nil), forCellReuseIdentifier: "ReusableXibCell")
    }
    
    // load more data when scroll
    func laodMoreData() {
        for _ in 0...9 {
            paginationVM.cells.append(PaginationModel<Any>(value: PaginationInfo(title: "New Loaded Pagination Data", bgColor: .purple)))
        }
        tableView.reloadData()
    }
    
    // create Loading view
    func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 150))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
    
}


// MARK: - UITableView
extension PaginationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paginationVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.paginationVM.cells[indexPath.row].value as! PaginationInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableXibCell", for: indexPath) as! ReusableXibCell
        cell.titleLabel.text = data.title
        cell.containerView.backgroundColor = data.bgColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if paginationVM.cells.count < 50 {
            if indexPath.row == paginationVM.cells.count - 1 {
                self.tableView.tableFooterView = createSpinnerFooter()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.tableView.tableFooterView = nil
                    self.laodMoreData()
                }
            }
        } 
    }
}
