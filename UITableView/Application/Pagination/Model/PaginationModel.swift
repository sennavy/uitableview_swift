//
//  PaginationModel.swift
//  UITableView
//
//  Created by Navy on 27/9/22.
//

import Foundation
import UIKit

// big array model to store all data
struct PaginationModel <T> {
    var value        : T?
}

// properties of each data
struct PaginationInfo {
    var title   : String
    var bgColor : UIColor
}
