//
//  CustomPopupVC.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import UIKit

// completion
typealias Completion_String_String  = (String, String)  -> Void

class CustomPopupVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var imgLogo              : UIImageView!
    @IBOutlet weak var lblTitle             : UILabel!
    @IBOutlet weak var addButton            : UIButton!
    @IBOutlet weak var nameTextField        : UITextField!
    @IBOutlet weak var phoneNumberTextField : UITextField!
    
    
    // MARK: - Variables
    var saveCompletion      : Completion_String_String = {_,_ in}
    var name                = ""
    var phoneNumber         = ""
    var profile             = ""
    var titleString         = "Add New Contact"
    var isEdit              = false


    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Functions
    func alertIncorrectInput() {
        let alert = UIAlertController(title: "Incorrect Input", message: "Please check contact info again", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupView() {
        nameTextField.becomeFirstResponder()
        addButton.layer.cornerRadius = 6
        imgLogo.layer.cornerRadius = 45
        nameTextField.text = name
        phoneNumberTextField.text = phoneNumber
        lblTitle.text = titleString
        
        if profile != "" {
            imgLogo.image = UIImage(named: profile)
        }
        
        if isEdit {
            addButton.setTitle("SAVE", for: .normal)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // MARK: - Objective-C
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - 250
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @IBAction func addContactButtonDidTap(_ sender: UIButton) {
        if nameTextField.text != "" && phoneNumberTextField.text != "" {
            self.saveCompletion(nameTextField.text ?? "", phoneNumberTextField.text ?? "")
            self.dismiss(animated: true)
            
        } else {
            alertIncorrectInput()
        }
    }
    
    
    @IBAction func backgroundButtonDidTap(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

