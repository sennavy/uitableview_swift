//
//  ProfileCell.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImagView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfileImagView.layer.cornerRadius = 35
    }
    
    func configCell(data: ContactInfo) {
        self.nameLabel.text = data.contact_name
        self.phoneNumberLabel.text = data.phone_number
        self.userProfileImagView.image = UIImage(named: data.profile_img)
    }

}
