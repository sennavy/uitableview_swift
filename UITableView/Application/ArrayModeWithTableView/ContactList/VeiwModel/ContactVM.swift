//
//  ContactVM.swift
//  UITableView
//
//  Created by Navy on 24/8/22.
//

import Foundation

class ContactVM {
    
    // variables
    var cells : [ContactModel<Any>] = []

    // functions
    func initCellData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        
        // profile cell (mobile owner info)
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "010 333 482", contact_name: "John Wick", profile_img: "contact_0", rowType: .Profile)))
        
        // contact list cells
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "010 463 331", contact_name: "Robert De Niro", profile_img: "contact_1", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "010 533 382", contact_name: "Joaquin Phoenix", profile_img: "contact_2", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "010 603 338", contact_name: "Al Pacino", profile_img: "contact_3", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "010 623 330", contact_name: "Liam Nesson", profile_img: "contact_4", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Gal Gadot", profile_img: "contact_6", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Johnny Depp", profile_img: "contact_7", rowType: .Contact)))
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Tom Cruise", profile_img: "contact_8", rowType: .Contact)))
        
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Audrey Hepburn", profile_img: "contact_9", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Emily Blunt", profile_img: "contact_10", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Zhao Liying", profile_img: "contact_11", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Ana de Armas", profile_img: "contact_12", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Angela Baby", profile_img: "contact_13", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Jet Li", profile_img: "contact_14", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Stephen Chow", profile_img: "contact_15", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Wind", profile_img: "contact_16", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Cloud", profile_img: "contact_17", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Pu Mab", profile_img: "contact_18", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Donnie Yen", profile_img: "contact_19", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Clara Sann", profile_img: "contact_20", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Kelly Bryan", profile_img: "contact_21", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Koshka Montega", profile_img: "contact_22", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "James Robin", profile_img: "contact_23", rowType: .Contact)))
        // cells.append(ContactInfoModel<Any>(value: ContactInfo(phone_number: "012 345 689", contact_name: "Andrew Brooklyn", profile_img: "contact_24", rowType: .Contact)))
    }
    
    // Add new contact
    func addNewContact(name: String, phoneNumber: String) {
        cells.append(ContactModel<Any>(value: ContactInfo(phone_number: phoneNumber, contact_name: name, profile_img: "contact_default", rowType: .Contact)))
    }
    
    // Edit contact
    func editContact(name: String, phoneNumber: String, index: Int) {
        var obj = cells[index].value as! ContactInfo
        obj.contact_name = name
        obj.phone_number = phoneNumber
        cells[index].value = obj
    }
    
}

