//
//  PrepareForReuseModel.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import Foundation


struct PrepareForReuseModel<T> {
    var value : T?
}

// properties of each menu
struct PrepareForReuseInfo {
    var name   : String
    var career : String
    var prfile : String
}
