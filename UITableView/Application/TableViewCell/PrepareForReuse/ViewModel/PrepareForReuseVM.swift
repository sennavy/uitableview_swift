//
//  PrepareForReuseVM.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import Foundation


class PrepareForReuseVM {
    // variables
    var cells : [PrepareForReuseModel<Any>] = []
    
    // functions
    func initData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Ben Ten", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "James Bones", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Rey Mysterio", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Sin Cara", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Rey Mysterio", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Johnyy Dept", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Amber Heard", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Bruce Lee", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Jackie Chan", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Donnie Yen", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "John Cena", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Ben Ten", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "James Bones", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Rey Mysterio", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Sin Cara", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Rey Mysterio", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Johnyy Dept", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Amber Heard", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Bruce Lee", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Jackie Chan", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "Donnie Yen", career: "Actor", prfile: "contact_1")))
        cells.append(PrepareForReuseModel<Any>(value: PrepareForReuseInfo(name: "John Cena", career: "Actor", prfile: "contact_1")))

    }
}
