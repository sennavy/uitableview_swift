//
//  PrepareForReuseVC.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import UIKit

class PrepareForReuseVC: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variables
    let prepareForReuseVM = PrepareForReuseVM()

    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareForReuseVM.initData()
    }
}


// MARK: - UITableView
extension PrepareForReuseVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prepareForReuseVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = prepareForReuseVM.cells[indexPath.row].value as! PrepareForReuseInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrepareForReuseCell", for: indexPath) as! PrepareForReuseCell
        // incase you want to test with data
//        cell.configCell(name: data.name, career: data.career, image: data.prfile)
        return cell
    }
    
    
    
}
