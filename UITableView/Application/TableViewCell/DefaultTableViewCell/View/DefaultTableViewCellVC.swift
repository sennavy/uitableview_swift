//
//  DefaultTableViewCellVC.swift
//  UITableView
//
//  Created by Navy on 7/10/22.
//

import UIKit

class DefaultTableViewCellVC: UIViewController {
    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    let contactVM = ContactVM()
    
    // MARK: - LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        contactVM.initCellData()
    }
    
}

// MARK: - UITableView
extension DefaultTableViewCellVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = contactVM.cells[indexPath.row].value as! ContactInfo
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = data.contact_name
        cell.imageView?.image = UIImage(named: "contact_007")
        
        return cell
    }
    
}
