//
//  MessengerModel.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import Foundation

// big array model to store all menu
struct MessengerModel <T> {
    var value        : T?
}

// properties of each menu
struct MessengerInfo {
    var name          : String
    var message       : String
    var newMsgCount   : Int
    var dateTime      : String
    var profileImg    : String
}
