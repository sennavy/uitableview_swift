//
//  SwipeForOptionCell.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import UIKit

class SwipeForOptionCell: UITableViewCell {
    
    
    @IBOutlet weak var profileImage         : UIImageView!
    @IBOutlet weak var nameLabel            : UILabel!
    @IBOutlet weak var messageLabel         : UILabel!
    @IBOutlet weak var newMessageCountLabel : UILabel!
    @IBOutlet weak var bageCountView        : UIView!
    
 
    func configCell(profile: String, name: String, message: String, unreadCount: Int) {
        profileImage.image        = UIImage(named: profile)
        nameLabel.text            = name
        messageLabel.text         = message
        newMessageCountLabel.text = "\(unreadCount)"
        
        if unreadCount > 0 {
            messageLabel.textColor = .black
            bageCountView.isHidden = false
        } else {
            bageCountView.isHidden = true
            messageLabel.textColor = .darkGray
        }
    }
}
