//
//  MessengerVM.swift
//  UITableView
//
//  Created by Navy on 29/9/22.
//

import Foundation

class MessengerVM {
    // variables
    var cells : [MessengerModel<Any>] = []
    
    // functions
    func initData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Angella Baby", message: "យប់ហ្នឹងម៉ាក់អូន អត់នៅផ្ទះទេ", newMsgCount: 1, dateTime: "Just now",profileImg: "chat_1")))
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Tom Cruise", message: "ផឹកអត់?", newMsgCount: 1, dateTime: "Just now",profileImg: "contact_8")))
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Johnny Depp", message: "You sent a sticker", newMsgCount: 0, dateTime: "Just now",profileImg: "contact_7")))
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Leo Messi", message: "តោះដាក់មួយប្រកួតហី?", newMsgCount: 2, dateTime: "Just now",profileImg: "chat_3")))
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Cristiano Ronaldo", message: "ប្រាប់ Messi អោយចេញថ្លៃទឹកសុទ្ធផង", newMsgCount: 1, dateTime: "",profileImg: "chat_4")))
        cells.append(MessengerModel<Any>(value: MessengerInfo(name: "Neymar JR", message: "ប្រូ លេងបាល់តាណើបម្លេះ?", newMsgCount: 1, dateTime: "Just now",profileImg: "chat_5")))
        
    }
    
    // Mark message as read
    func markAsRead(index: Int, newMsgCount: Int) {
        var obj = cells[index].value as! MessengerInfo
        obj.newMsgCount = newMsgCount
        cells[index].value = obj
    }
    
}
