//
//  IndentationVC.swift
//  UITableView
//
//  Created by Navy on 30/9/22.
//

import UIKit

class IndentationVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variable
    let indentationVM   = IndentationVM()
    
    
    // MARK: - LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        indentationVM.initData()
    }
}

// MARK: - UITableView
extension IndentationVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return indentationVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let data = indentationVM.cells[section].value as! IndentationInfo
        return data.header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dataInSection = indentationVM.cells[section].value as! IndentationInfo
        return dataInSection.name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataInSection = indentationVM.cells[indexPath.section].value as! IndentationInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "IndentationCell", for: indexPath) as! IndentationCell
        cell.nameLabel.text = dataInSection.name[indexPath.row]
        return cell
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        // Save all header title for Map
        var mapStringArray = [""]
        for i in 0...indentationVM.cells.count - 1 {
            let dataInSection = indentationVM.cells[i].value as! IndentationInfo
            mapStringArray.append("\(dataInSection.header)")
        }
        // Apply all headers on tableView mapping
        return mapStringArray.compactMap({$0})
    }
    
  
}
