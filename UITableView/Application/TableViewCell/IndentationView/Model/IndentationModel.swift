//
//  IndentationModel.swift
//  UITableView
//
//  Created by Navy on 3/10/22.
//

import Foundation

// big array model to store all contacts
struct IndentationModel <T> {
    var value        : T?
}

// properties of each contact
struct IndentationInfo {
    var header  : String
    var name    : [String]
}
