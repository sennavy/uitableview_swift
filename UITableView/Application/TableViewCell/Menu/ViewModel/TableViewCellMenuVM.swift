//
//  TableViewCellMenuVM.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import Foundation

class TableViewCellMenuVM {
    // variables
    var cells : [TableViewCellMenuModel<Any>] = []
    
    // functions
    func initCellData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.1  AwakeFromNib()", rowType: .AwakeFromNib)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.2  PrepareForReuse()", rowType: .PrepareForReuse)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.3  SetSelected()", rowType: .SetSelected)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.4  Indenation View", rowType: .IndenationView)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.5  Swipe Left & Right", rowType: .SwipeLeftAndRight)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.6  Move Row", rowType: .MoveRow)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.7  Move Section", rowType: .MoveSection)))
        cells.append(TableViewCellMenuModel<Any>(value: TableViewCellMenuInfo(title: "8.8  Default Cell of TableView", rowType: .DefaultTableViewCell)))

    }
    
}
