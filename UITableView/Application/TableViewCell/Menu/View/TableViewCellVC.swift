//
//  TableViewCellVC.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit



enum TableViewCellMenu: Int, CaseIterable {
    case AwakeFromNib
    case PrepareForReuse
    case SetSelected
    case IndenationView
    case SwipeLeftAndRight
    case MoveRow
    case MoveSection
    case DefaultTableViewCell
}

class TableViewCellVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variable
    let tableViewCellMenuVM = TableViewCellMenuVM()
    
    
    // MARK: - LifeCyle
    override func viewDidLoad() {
        tableViewCellMenuVM.initCellData()
        super.viewDidLoad()
    }
}

// MARK: - UITableView
extension TableViewCellVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableViewCellMenuVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = tableViewCellMenuVM.cells[indexPath.row].value as! TableViewCellMenuInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellMenuCell", for: indexPath) as! TableViewCellMenuCell
        cell.titleLabel.text = data.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = tableViewCellMenuVM.cells[indexPath.row].value as! TableViewCellMenuInfo
        switch data.rowType {
            
        case .AwakeFromNib:
            let vc = UIStoryboard.init(name: "AwakeFromNibSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "AwakeFromNibVC") as? AwakeFromNibVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .SetSelected:
            let vc = UIStoryboard.init(name: "SetSelectedSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "SetSelectedVC") as? SetSelectedVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .PrepareForReuse:
            let vc = UIStoryboard.init(name: "PrepareForReuseSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "PrepareForReuseVC") as? PrepareForReuseVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .IndenationView:
            let vc = UIStoryboard.init(name: "IndentationSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "IndentationVC") as? IndentationVC
            self.navigationController?.pushViewController(vc!, animated: true)

        case .SwipeLeftAndRight:
            let vc = UIStoryboard.init(name: "MessengerSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "MessengerVC") as? MessengerVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .MoveRow:
            let vc = UIStoryboard.init(name: "MoveRowSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "MoveRowVC") as? MoveRowVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .MoveSection:
            let vc = UIStoryboard.init(name: "MoveSectionSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "MoveSectionVC") as? MoveSectionVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        case .DefaultTableViewCell:
            let vc = UIStoryboard.init(name: "DefaultTableViewCellSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "DefaultTableViewCellVC") as? DefaultTableViewCellVC
            self.navigationController?.pushViewController(vc!, animated: true)
        
        }
    }
    
    
}
