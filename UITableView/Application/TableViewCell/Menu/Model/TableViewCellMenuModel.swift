//
//  TableViewCellMenuModel.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import Foundation

// big array model to store all menu
struct TableViewCellMenuModel <T> {
    var value        : T?
}

// properties of each menu
struct TableViewCellMenuInfo {
    var title   : String
    var rowType : TableViewCellMenu
}
