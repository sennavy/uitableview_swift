//
//  AwakeFromNibCell.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit

class AwakeFromNibCell: UITableViewCell {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var middleLabel: UILabel!
    
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        // Init Left View
        self.leftView.backgroundColor = .systemGreen
        self.leftLabel.text = "SQUARE"
        self.leftLabel.textColor = .systemGreen
        
        
        // Init Middle View
        self.middleView.backgroundColor = .red
        self.middleLabel.text = "RECTANGLE"
        self.middleLabel.textColor = .red
        
        // Init Right View
        self.rightView.cornerRadius = 40
        self.rightView.backgroundColor = .blue
        self.rightLabel.text = "CIRCLE"
        self.rightLabel.textColor = .blue

        
    }

}
