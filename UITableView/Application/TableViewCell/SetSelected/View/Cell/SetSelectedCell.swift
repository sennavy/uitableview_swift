//
//  SetSelectedCell.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit

class SetSelectedCell: UITableViewCell {
    
    
    @IBOutlet weak var radioButtonImg   : UIImageView!
    @IBOutlet weak var paymentName      : UILabel!
    @IBOutlet weak var paymentIconImg   : UIImageView!
    @IBOutlet weak var containerView    : UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Noted : We use setSelected() method to change the appearance of contents inside the selected cell such as labels, image, and background.
    //         When the selected state of a cell is true, the appearance of contents wil change.
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            radioButtonImg.image = UIImage(named: "radio_on")
            paymentName.textColor = UIColor.init(hexString: "248FEA")
            containerView.backgroundColor = UIColor.init(hexString: "D8F4FF")
        } else {
            radioButtonImg.image = UIImage(named: "radio_off")
            paymentName.textColor = .black
            containerView.backgroundColor = .white
        }
    }
    
    func configCell(data: PaymentMethodInfo) {
        self.paymentName.text = data.title
        self.paymentIconImg.image = UIImage(named: data.icon)
    }

}
