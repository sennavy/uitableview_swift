//
//  SetSelectedVC.swift
//  UITableView
//
//  Created by Navy on 28/9/22.
//

import UIKit

class SetSelectedVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variable
    let paymentMethodVM = PaymentMethodVM()
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentMethodVM.initCellData()
    }
}

// MARK: - UITableView
extension SetSelectedVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethodVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = paymentMethodVM.cells[indexPath.row].value as! PaymentMethodInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetSelectedCell", for: indexPath) as! SetSelectedCell
        cell.configCell(data: data)
        return cell
    }
    
    
}
