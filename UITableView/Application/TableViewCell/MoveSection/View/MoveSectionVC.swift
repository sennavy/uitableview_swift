//
//  MoveSectionVC.swift
//  UITableView
//
//  Created by Navy on 5/10/22.
//

import UIKit

class MoveSectionVC: UIViewController {

    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    
    // MARK: - Variable
    let moveDataVM = MoveSectionVM()
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moveDataVM.initData()
        self.tableView.isEditing = false
        editBarButton.title = "Edit"
    }
    
    
    @IBAction func editBarButtonDidTap(_ sender: UIBarButtonItem) {
        self.tableView.isEditing = !self.tableView.isEditing
        self.editBarButton.title = self.tableView.isEditing ? "Done" : "Edit"
    }
    
}
 

// MARK: - UITableView
extension MoveSectionVC: UITableViewDelegate, UITableViewDataSource {
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return moveDataVM.cells.count
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = moveDataVM.cells[indexPath.section].value as! MoveSectionInfo

        let cell = tableView.dequeueReusableCell(withIdentifier: "MoveSectionCell", for: indexPath) as! MoveSectionCell
        cell.configCell(title: data.title)
        return cell
    }
    
}


extension MoveSectionVC {


    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        guard sourceIndexPath != destinationIndexPath else {
            return
        }
        
        let movedObject = self.moveDataVM.cells[sourceIndexPath.section]
        self.moveDataVM.cells.remove(at: sourceIndexPath.section)
        self.moveDataVM.cells.insert(movedObject, at: destinationIndexPath.section)
        
        print("Moved from SECTION \(sourceIndexPath.section) to SECTION \(destinationIndexPath.section)")
    }
    
}
    
