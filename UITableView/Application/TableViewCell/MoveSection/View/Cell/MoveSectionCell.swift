//
//  MoveSectionCell.swift
//  UITableView
//
//  Created by Navy on 5/10/22.
//

import UIKit

class MoveSectionCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    func configCell(title: String) {
        titleLabel.text = title
    }

}
