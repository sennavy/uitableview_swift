//
//  MoveSectionModel.swift
//  UITableView
//
//  Created by Navy on 6/10/22.
//

import Foundation

// big array to contain all data
struct MoveSectionModel <T> {
    var value        : T?
}

// properties of each section
struct MoveSectionInfo {
    var title : String
}
