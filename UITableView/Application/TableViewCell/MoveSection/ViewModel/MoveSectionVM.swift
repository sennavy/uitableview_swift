//
//  MoveDataVM.swift
//  UITableView
//
//  Created by Navy on 6/10/22.
//

import Foundation


class MoveSectionVM {
    
    // variables
    var cells : [MoveSectionModel<Any>] = []

    // functions
    func initData() {
       
       cells = [] // Must clear array before append new values, to prevent data redundancy
       
       for i in 0...9 {
           cells.append(MoveSectionModel<Any>(value: MoveSectionInfo(title: "SECTION \(i)")))
       }
    }

}
