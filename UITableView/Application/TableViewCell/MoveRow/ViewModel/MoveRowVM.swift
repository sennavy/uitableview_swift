//
//  MoveRowVM.swift
//  UITableView
//
//  Created by Navy on 7/10/22.
//

import Foundation


class MoveRowVM {
    
    // variables
    var cells : [MoveRowModel<Any>] = []

    // functions
    func initData() {
       
       cells = [] // Must clear array before append new values, to prevent data redundancy
       
       for i in 0...9 {
           cells.append(MoveRowModel<Any>(value: MoveRowInfo(title: "ROW \(i)")))
       }
    }

}
