//
//  MoveRowCell.swift
//  UITableView
//
//  Created by Navy on 6/10/22.
//

import UIKit

class MoveRowCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configCell(title: String) {
        titleLabel.text = title
    }


}
