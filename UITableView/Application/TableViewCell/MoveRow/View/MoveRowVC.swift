//
//  MoveRowVC.swift
//  UITableView
//
//  Created by Navy on 6/10/22.
//

import UIKit

class MoveRowVC: UIViewController {

    @IBOutlet weak var editBarButtonItem    : UIBarButtonItem!
    @IBOutlet weak var tableView            : UITableView!
    
    let moveRowVM = MoveRowVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moveRowVM.initData()
        self.editBarButtonItem.title = "Edit"
    }
    

    @IBAction func editBarButtonDidTap(_ sender: UIBarButtonItem) {
        self.tableView.isEditing = !tableView.isEditing
        self.editBarButtonItem.title = self.tableView.isEditing ? "Done" : "Edit"
        
    }
    
}


extension MoveRowVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        moveRowVM.cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = moveRowVM.cells[indexPath.row].value as! MoveRowInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoveRowCell", for: indexPath) as! MoveRowCell
        cell.configCell(title: data.title)
        return cell
    }
    
    
    // Use these two function to hide delete Button when tableVeiw.isEditing = true
    // ============================================================================
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    // =============================================================================
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        guard sourceIndexPath != destinationIndexPath else {
            return
        }
        
        let movedObject = self.moveRowVM.cells[sourceIndexPath.row]
        self.moveRowVM.cells.remove(at: sourceIndexPath.row)
        self.moveRowVM.cells.insert(movedObject, at: destinationIndexPath.row)
        
        print("Moved from ROW \(sourceIndexPath.row) to ROW \(destinationIndexPath.row)")
    }
    
    
}
