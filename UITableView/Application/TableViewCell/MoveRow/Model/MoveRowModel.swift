//
//  MoveRowModel.swift
//  UITableView
//
//  Created by Navy on 7/10/22.
//

import Foundation


// big array to contain all data
struct MoveRowModel <T> {
    var value        : T?
}

// properties of each Row
struct MoveRowInfo {
    var title : String
}
