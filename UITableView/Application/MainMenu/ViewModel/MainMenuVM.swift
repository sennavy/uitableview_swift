//
//  MainMenuVM.swift
//  UITableView
//
//  Created by Navy on 14/9/22.
//

import Foundation

class MainMenuVM {
    // variables
    var cells : [MainMenuModel<Any>] = []
    
    // functions
    func initCellData() {
        
        cells = [] // Must clear array before append new values, to prevent data redundancy
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "1. TableView with Array Data", rowType: .ArrayModeWithTableView)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "2. Add, Delete & Update TableView", rowType: .AddDeleteUpdateRow)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "3. Loading Data Animation", rowType: .LoadDataAnimation)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "4. CollectionView Inside TableView", rowType: .CollectionInsideTable)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "4. Header And Footer of TableView", rowType: .HeaderAndFooter)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "5. Reuse Cell from Xib File", rowType: .ReusableXibFile)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "6. Pagination With TableView", rowType: .Pagination)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "7. TableView Sections", rowType: .TableViewSection)))
        cells.append(MainMenuModel<Any>(value: MenuInfo(title: "8. TableView Cell", rowType: .TableViewCell)))

    }
    
}
