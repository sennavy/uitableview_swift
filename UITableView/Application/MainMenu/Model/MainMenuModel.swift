//
//  MainMenuModel.swift
//  UITableView
//
//  Created by Navy on 14/9/22.
//

import Foundation

enum ItemType: String, CaseIterable {
    case ArrayModeWithTableView
    case AddDeleteUpdateRow
    case LoadDataAnimation
    case CollectionInsideTable
    case TableViewSection
    case HeaderAndFooter
    case ReusableXibFile
    case Pagination
    case TableViewCell
}

// big array model to store all menu
struct MainMenuModel <T> {
    var value        : T?
}

// properties of each menu
struct MenuInfo {
    var title   : String
    var rowType : ItemType
}
